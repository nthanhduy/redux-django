import axios from 'axios';
import { GET_LEADS, DELETE_LEADS, ADD_LEAD, GET_ERRORS } from './type';
import { createMessage } from './messages';

//GET LEADS
 export const getLeads = () => dispatch => {
    axios.get('/api/leads')
        .then(res => {
            dispatch({
                type: GET_LEADS,
                payload: res.data
            })
        }).catch(err => console.log(err))
 };

 //DELETE LEADS
 export const deleteLead = (id) => dispatch => {
    axios.delete(`/api/leads/${id}/`)
        .then(res => {
            dispatch(createMessage({
                deleteLead: 'Lead Deleted'
            }))
            dispatch({
                type: DELETE_LEADS,
                payload: id
            })
        }).catch(err => console.log(err))
 };

  //ADD LEADS
  export const addLead = (lead) => dispatch => {
    axios.post('/api/leads/', lead)
        .then(res => {
            dispatch(createMessage({
                addLead: 'Lead added successfully'
            }))
            dispatch({
                type: ADD_LEAD,
                payload: res.data
            })
        }).catch(err => {
            const errors = {
                msg: err.response.data,
                status: err.response.status
            };
            dispatch({
                type: GET_ERRORS,
                payload: errors
            })
        })
 };