import { CREATE_MESSAGE } from './type';

export const createMessage = msg => {
    return {
        type: CREATE_MESSAGE,
        payload: msg
    }
}