import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';//syntax: ipmt
import { addLead } from '../../actions/leads';


export class Form extends Component {
  state = {
    name: '',
    email: '',
    message: ''
  };

  static propTypes = {
    addLead: PropTypes.func.isRequired
  };

  onChange = e => this.setState({ [e.target.name]:e.target.value });
  onSubmit = e => {
    e.preventDefault();
    const { name, email, message } = this.state
    const lead = { name, email, message };
    this.props.addLead(lead)
    this.setState({
      name: "",
      email: "",
      message: ""

    })
  };
  render() {
    const { name, email, message } = this.state
    return (
      <div>
      <h1>Add Lead Form</h1>
      <form onSubmit={this.onSubmit}>
      <div className="form-group row">
        <label htmlFor="name" className="col-sm-2 col-form-label">Name</label>
          <div className="col-sm-10">
            <input type="text"
             className="form-control" 
             id="name"
             name="name"
             placeholder="Name"
             onChange={this.onChange}
             value={name}/>
          </div>
      </div>
      <div className="form-group row">
        <label className="col-sm-2 col-form-label">Email</label>
          <div className="col-sm-10">
            <input type="text"
            className="form-control"
            id="email"
            name="email"
            placeholder="Email"
            onChange={this.onChange}
            value={email}/>
          </div>
      </div>
      <div className="form-group row">
        <label className="col-sm-2 col-form-label">Message</label>
          <div className="col-sm-10">
            <input type="text"
            className="form-control"
            id="message"
            name="message"
            placeholder="Message"
            onChange={this.onChange}
            value={message}/>
          </div>
      </div>
        <div className="form-group row">
          <div className="col-sm-10">
            <button type="submit" className="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
      </div>
    )
  }
}

export default connect(null, { addLead })(Form);
